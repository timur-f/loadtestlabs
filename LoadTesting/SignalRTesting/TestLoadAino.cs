﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using SignalRTesting.AinoService;

namespace SignalRTesting
{
    public class TestLoadAino
    {
        private const string AinoConnString = "Data Source=vpn_mssqllab01.indemandterp.com;Initial Catalog=ainostaging;Persist Security Info=False;MultipleActiveResultSets=true;User ID=sa;Password=Ind3m@nd!; Connection Timeout=30;Min Pool Size=200;Max Pool Size=500;";
        private readonly AINOServiceSoapClient _ainoService;

        public TestLoadAino()
        {
            var binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.None;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.SendTimeout = new TimeSpan(0, 0, 10);
            //var endpoint = new EndpointAddress("http://localhost:21661/ainoservice.asmx");
            var endpoint = new EndpointAddress("http://vpn_webstage01.indemandterp.com:8099/ainoservice.asmx");

            //_ainoService = new AINOServiceSoapClient(binding, endpoint);
            _ainoService = new AINOServiceSoapClient();

            //var userType = _ainoService.GetUserType("loadtestcart01");
        }

        public async Task StartLoadTest(List<string> users, string defaultPassword)
        {
            Console.WriteLine("##### Authentication process started......");

            var taskList = new List<Task>();

            //await WarmUpAinoServicesSqlConnectionPool(users.First());

            for (int i = 0; i < 100; i++)
            {
                taskList.AddRange(users.Select(userName => AinoAuthUserAsync(userName, defaultPassword)));
                await Task.WhenAll(taskList);
                Console.WriteLine($"#################### End of round {i} ########################");
            }

            Console.WriteLine("##### Authentication results ended......");
        }

        private async Task<User> AinoAuthUserAsync(string userName, string password)
        {
            var stopwatch = new Stopwatch();
            Console.WriteLine($"Authenticating account: {userName}...");
            stopwatch.Start();

            var user = await _ainoService.AuthenticateUserAsync(userName, password, string.Empty).ConfigureAwait(false);
            stopwatch.Stop();
            Console.WriteLine($"Account: {userName} authenticated. Result: {user.Authenticated}. Elapsed time: {stopwatch.ElapsedMilliseconds}.");
            return user;
        }

        private async Task AinoGetUserContactsAsync(string userName)
        {
            var stopwatch = new Stopwatch();
            Console.WriteLine($"Geting User Contacts: {userName}...");
            stopwatch.Start();
            var result = await _ainoService.GetContactsAsync(userName).ConfigureAwait(false);
            stopwatch.Stop();
            Console.WriteLine($"User: {userName}. Contacts: {result.Length}. Elapsed time: {stopwatch.ElapsedMilliseconds}.");
        }

        private async Task WarmUpAinoServicesSqlConnectionPool(string userName)
        {
            Console.WriteLine("Warming up connection pooling...");
            await AinoGetUserContactsAsync(userName);
            Console.WriteLine("Warming up... Press a key when ready to proceed...");
            Console.ReadKey();
            Console.WriteLine("Warming up complete");
        }

        private async Task WarmUpSqlConnectionPool()
        {
            Console.WriteLine("Warming up connection pooling...");
            using (SqlConnection connection = new SqlConnection(AinoConnString))
            {
                Console.WriteLine("Opening a test connection...");
                await connection.OpenAsync();
                Console.WriteLine("Test connection initiated");
            }
            Console.WriteLine("Warming up... Press a key when ready to proceed...");
            Console.ReadKey();
            Console.WriteLine("Warming up complete");
        }

        private async Task AinoDirectSqlExecutionOfGetUserTypeAsync(string userName)
        {
            //const string connString = "Data Source=vpn_mssqllab01.indemandterp.com;Initial Catalog=ainostaging;Persist Security Info=False;MultipleActiveResultSets=true;User ID=sa;Password=Ind3m@nd!";
            //const string connString = "Data Source=indemandsql.database.windows.net;Initial Catalog=loadtest;Persist Security Info=False;MultipleActiveResultSets=true;User ID=indemand_admin;Password=@dm!nSql;";

            var stopwatch = new Stopwatch();
            Console.WriteLine($"Geting User Type: {userName}...");
            stopwatch.Start();

            int userType;

            using (SqlConnection connection = new SqlConnection(AinoConnString))
            using (SqlCommand command = new SqlCommand())
            {
                connection.StatisticsEnabled = true;
                await connection.OpenAsync();

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "GetUserType";
                command.Connection = connection;
                command.Parameters.Add("oUsername", SqlDbType.NVarChar).Value = userName;

                var reader = await command.ExecuteReaderAsync();
                await reader.ReadAsync();
                userType = await reader.GetFieldValueAsync<int>(0);

                var statistics = connection.RetrieveStatistics();
                Console.WriteLine($"SQL STATS: ConnectionTime:{statistics["ConnectionTime"]}; ExecutionTime:{statistics["ExecutionTime"]}; NetworkServerTime:{statistics["NetworkServerTime"]}.");
            }

            stopwatch.Stop();
            Console.WriteLine($"User: {userName}. User Type: {userType}. Elapsed time: {stopwatch.ElapsedMilliseconds}.");
        }
    }
}
