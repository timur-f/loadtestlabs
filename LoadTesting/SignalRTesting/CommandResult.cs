namespace SignalRTesting
{
    public class CommandResult
    {
        public enum Destination { CALLING_USER, GROUP, USER, ALL_LOGGED_IN_USERS, INTERNAL }
        public object Data { get; set; }
        public Destination Destiny { get; set; }
        public string UserConnId { get; set; }
        public string GroupId { get; set; }

        public CommandResult()
        {
            Destiny = Destination.CALLING_USER;
        }


    }
}