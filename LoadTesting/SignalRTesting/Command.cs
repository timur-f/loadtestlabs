﻿using Newtonsoft.Json;

namespace SignalRTesting
{
    public class Command
    {
        [JsonProperty(PropertyName = "data", Order = 2)]
        public object Data { get; set; }
        [JsonProperty(Order = 1, PropertyName = "IDI_CMD")]
        public CommandId Id { get; set; }
    }
}