﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Client;
using Newtonsoft.Json.Linq;

namespace SignalRTesting
{
    internal static class Program
    {
        private const string CommandHubName = "CommandHub";
        private const string DefaultPassword = "Velocity01!";

        private static void Main()
        {
            Console.WriteLine("Starting...");
            Console.WriteLine($"ServicePointManager.DefaultConnectionLimit: {ServicePointManager.DefaultConnectionLimit}");

            //https://vportalload01.indemandterp.com

            //const string url = "https://vdsstage01.indemandterp.com:17990/";
            //const string url = "http://*:8080/";
            //const string url = "http://localhost:8080/";
            const string url = "https://vdsprd01.indemandterp.com:17990/";
            //const string url = "http://192.168.50.132:8080/";
            HubConnection connection = new HubConnection(url);

            const int first = 1;
            const int count = 100;
            const string cartPattern = "loadtestcart";
            const string terpPattern = "loadtestterp";


            var carts = Enumerable.Range(first, count).Select(i => i < 10 ? "0" + i : i.ToString()).Select(i => cartPattern + i).Except(new [] {"loadtestcart21"});
            //var carts = Enumerable.Range(first, count).Select(i => i < 10 ? "0" + i : i.ToString()).Select(i => cartPattern + i);
            var terps = Enumerable.Range(first, count).Select(i => i < 10 ? "0" + i : i.ToString()).Select(i => terpPattern + i);
            var users = carts.Concat(terps).ToList();

            //Generate same users
            //var users = Enumerable.Repeat("loadtestterp100", 200).ToList();

            Console.WriteLine($"Generated {users.Count} number of users.");
            //AuthVidyoUsers(users);
            //ConnectUsers(users, url).GetAwaiter().GetResult();

            /* login */
            //Console.WriteLine("Trying to login...");
            //ConnectUserForLogin(url, users[1], DefaultPassword).GetAwaiter().GetResult();

            //AuthVidyoUsers(users);

            //users.Clear();
            //users.Add("loadlocart89");
            //ConnectUsers(users, url).GetAwaiter().GetResult();

            // Increase the number of users in the array by adding the same range
            //users.AddRange(users);

            var testLoadAino = new TestLoadAino();
            testLoadAino.StartLoadTest(users, DefaultPassword).GetAwaiter().GetResult();


            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
            
            connection.Dispose();
        }

        private static void AuthVidyoUsers(List<string> users)
        {
            var testVidyo = new TestVidyo();

            testVidyo.AuthenticateUsersAsync2(users, DefaultPassword).GetAwaiter().GetResult();
            //testVidyo.GetMembersAsync(users.First()).GetAwaiter().GetResult();
        }

        private static async Task ConnectUsers(List<string> users, string url)
        {
            List<Task> taskList = new List<Task>(users.Count);
            taskList.AddRange(users.Select(userName => ConnectUser(url, userName, DefaultPassword)));

            await Task.WhenAll(taskList);
        }

        private static async Task ConnectUser(string url, string userName, string password)
        {
            HubConnection connection = new HubConnection(url);

            IHubProxy proxy = connection.CreateHubProxy(CommandHubName);


            await connection.Start();

            //connection.ConnectionId
            string guid = Guid.NewGuid().ToString();
            var userIsConnectedCommand = CreateUserIsConnectedCommand(userName, password, guid);

            proxy.On<object>("Execute", obj =>
            {
                Command command = JObject.FromObject(obj).ToObject<Command>();
                if (command.Id == CommandId.II_CLIENT_OUT_EVENT_USER_IS_CONNECTED)
                {
                    Console.WriteLine(obj.ToString());

                    var loginCommand = CreateLoginCommand(userName, password, guid);
                    proxy.Invoke("ExecuteAsync", loginCommand);
                }
                else
                {
                    Console.WriteLine($"CommandId = {command.Id}; Data: {obj.ToString()}");
                }

            });
            await proxy.Invoke("Execute", userIsConnectedCommand);
        }

        private static async Task ConnectUserForLogin(string url, string userName, string password)
        {
            HubConnection connection = new HubConnection(url);
            IHubProxy proxy = connection.CreateHubProxy(CommandHubName);

            await connection.Start();

            proxy.On<object>("Execute", obj =>
            {
                Command command = JObject.FromObject(obj).ToObject<Command>();
                Console.WriteLine($"CommandId = {command.Id}; Data: {obj.ToString()}");
            });

            const string urlToLogin = "http://192.168.50.226:8080/api/login";
            const string method = "POST";
            const string contentType = "application/json; charset=utf-8";
            var jsonData = "{" +
                                    $" \"userName\":     \"{userName}\"," +
                                    $" \"password\":     \"{password}\" ," +
                                    $" \"browserGuid\":  \"{Guid.NewGuid()}\"," +
                                    $" \"connectionId\": \"{connection.ConnectionId}\"," +
                                     " \"force\":        \"false\"," +
                                     " \"commandCode\":  \"5000\"," + //II_CLIENT_IN_EVENT_LOGIN = 5000
                                     " \"clientType\":   \"Web\"," +
                                     " \"version\":      \"1.2.3\"," +
                                     " \"clientIP\":     \"192.168.1.1\"" +
                           "}";

            CreateHttpRequest(urlToLogin, method, contentType, jsonData);
        }

        private static async Task ConnectUserForLogout(string url, string userAgent)
        {
            HubConnection connection = new HubConnection(url);
            IHubProxy proxy = connection.CreateHubProxy(CommandHubName);

            await connection.Start();

            proxy.On<object>("Execute", obj =>
            {
                Command command = JObject.FromObject(obj).ToObject<Command>();
                Console.WriteLine($"CommandId = {command.Id}; Data: {obj.ToString()}");
            });

            const string urlToLogin = "http://192.168.50.226:8080/api/logout";
            const string method = "POST";
            const string contentType = "application/json; charset=utf-8";
            var jsonData = "{" +
                                    $" \"userAgent\":     \"{userAgent}\"," +
                                    $" \"connectionId\": \"{connection.ConnectionId}\"" +
                           "}";

            CreateHttpRequest(urlToLogin, method, contentType, jsonData);
        }

        private static Command CreateUserIsConnectedCommand(string userName, string password, string guid)
        {
            return new Command
            {
                Id = CommandId.II_CLIENT_IN_EVENT_USER_IS_CONNECTED,
                Data = new Dictionary<string, string>
                {
                    { "username", userName },
                    { "password", password },
                    { "browserGuid", guid },
                }
            };
        }

        private static Command CreateLoginCommand(string userName, string password, string guid)
        {
            return new Command
            {
                Id = CommandId.II_CLIENT_IN_EVENT_LOGIN,
                Data = new Dictionary<string, string>
                {
                    { "username", userName },
                    { "password", password },
                    { "browserGuid", guid },
                    { "clientType", "Web" },
                    { "version", "1.2.3" },
                },
            };
        }

        private static void CreateHttpRequest(string url, string method, string contentType, string jsonData = "")
        {
            WebRequest request = WebRequest.Create(url);
            request.Method = method;
            request.ContentType = contentType;

            if (jsonData != string.Empty)
            {
                request.ContentLength = jsonData.Length;
                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(jsonData);
                }
            }

            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        if (stream == null) return;
                        using (StreamReader streamReader = new StreamReader(stream))
                        {
                            var result = streamReader.ReadToEnd();
                            Console.WriteLine(result);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine(exception.InnerException.Message);
            }
        }
    }
}
