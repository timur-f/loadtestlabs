﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using Polly;
using SignalRTesting.VidyoPortalAdminService;
using SignalRTesting.VidyoPortalUserService;

namespace SignalRTesting
{
    public class TestVidyo
    {
        private EndpointAddress Endpoint { get; }
        public BasicHttpBinding Binding { get; }
        public TestVidyo()
        {
            Binding = new BasicHttpBinding();
            Binding.Security.Mode = BasicHttpSecurityMode.Transport;
            Binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;

            Endpoint = new EndpointAddress("https://vportallab01.indemandterp.com/services/v1_1/VidyoPortalUserService/");
        }

        public async Task AuthenticateUsersAsync(List<string> users, string password)
        {
            List<Task> taskList = new List<Task>(users.Count);

            Console.WriteLine($"Starting authentication for {users.Count} user(s)...");
            Console.WriteLine("Warming up...");
            AuthenticateUser(users.First(), password);
            Console.WriteLine("Warm up completed.");

            foreach (string user in users)
            {
                Task completionStatus = AuthenticateUserAsync(user, password);
                taskList.Add(completionStatus);
                //await Task.Delay(TimeSpan.FromMilliseconds(700));
            }

            //taskList.AddRange(users.Select(userName => AuthenticateUserAsync(userName, password)));

            await Task.WhenAll(taskList);
        }

        public async Task AuthenticateUsersAsync2(List<string> users, string password)
        {
            List<Task> taskList = new List<Task>(users.Count);

            Console.WriteLine($"Starting authentication for {users.Count} user(s)...");
            Console.WriteLine("Warming up...");
            //AuthenticateUser("loadtestcart21", password);
            AuthenticateUser(users.First(), password);
            Console.WriteLine("Warm up completed.");

            for (int i = 0; i < 1000; i++)
            {
                foreach (string user in users)
                {
                    Task task = Task.Factory.StartNew(() => AuthenticateUser(user, password), TaskCreationOptions.LongRunning);
                    taskList.Add(task);
                    //await Task.Delay(TimeSpan.FromMilliseconds(700));
                }

                await Task.WhenAll(taskList);

                Console.WriteLine($"=============================== Completed authentication round: {i}. ===================================");
            }

        }

        private void AuthenticateUser(string user, string password)
        {
            try
            {
                var stopwatch = new Stopwatch();
                Console.WriteLine($"[{DateTime.Now}] Authenticating user: '{user}'...");
                stopwatch.Start();

                BasicHttpBinding binding = new BasicHttpBinding();
                binding.Security.Mode = BasicHttpSecurityMode.Transport;
                binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                const int seconds = 5;
                binding.SendTimeout = TimeSpan.FromSeconds(seconds);
                binding.ReceiveTimeout = TimeSpan.FromSeconds(seconds);
                binding.OpenTimeout = TimeSpan.FromSeconds(seconds);
                binding.CloseTimeout = TimeSpan.FromSeconds(seconds);

                EndpointAddress endpoint = new EndpointAddress("https://indemand.beta.vidyo.com/services/v1_1/VidyoPortalUserService/");
                //EndpointAddress endpoint = new EndpointAddress("https://vportallab01.indemandterp.com/services/v1_1/VidyoPortalUserService/");
                //EndpointAddress endpoint = new EndpointAddress("https://vportalload01.indemandterp.com/services/v1_1/VidyoPortalUserService/");

                MyAccountResponse response;
                using (var portalClient = new VidyoPortalUserServicePortTypeClient(binding, endpoint))
                {
                    portalClient.ClientCredentials.UserName.UserName = user;
                    portalClient.ClientCredentials.UserName.Password = password;

                    response = portalClient.myAccount(new MyAccountRequest());

                    //response = portalClient.myAccount(new MyAccountRequest());
                    //var logInResponse = portalClient.logIn(new LogInRequest());
                    //logInResponse = portalClient.logIn(new LogInRequest());
                    //logInResponse = portalClient.logIn(new LogInRequest());
                }
                stopwatch.Stop();
                Console.WriteLine($"[{DateTime.Now}]                Complete for user: '{user}'! Result: {response.Entity.displayName}; Time taken: {stopwatch.Elapsed.TotalSeconds}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("[{DateTime.Now}]                 Error!");
                Console.WriteLine(ex.Message);
            }
        }

        private async Task AuthenticateUserAsync(string user, string password)
        {
            try
            {
                var stopwatch = new Stopwatch();
                Console.WriteLine($"[{DateTime.Now}] Authenticating user: '{user}'...");
                stopwatch.Start();
                using (var portalClient = new VidyoPortalUserServicePortTypeClient(Binding, Endpoint))
                {
                    portalClient.ClientCredentials.UserName.UserName = user;
                    portalClient.ClientCredentials.UserName.Password = password;
                    var response = await portalClient.logInAsync(new LogInRequest()).ConfigureAwait(false);
                    //myAccountResponse1 response = await portalClient.myAccountAsync(new MyAccountRequest()).ConfigureAwait(false);
                    //response.MyAccountResponse.Entity.MemberStatus.
                }
                stopwatch.Stop();
                Console.WriteLine($"[{DateTime.Now}]                Complete for user: '{user}'! Time taken: {stopwatch.Elapsed.TotalSeconds}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("[{DateTime.Now}]                 Error!");
                Console.WriteLine(ex.Message);
            }
        }

        public async Task GetMembersAsync(string userName)
        {
            const string adminUser = "dev_velocity";
            const string adminPassword = "Velocity01!";

            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;

            EndpointAddress endpoint = new EndpointAddress("https://vportalload01.indemandterp.com/services/v1_1/VidyoPortalAdminService/");

            var client = new VidyoPortalAdminServicePortTypeClient(binding, endpoint);
            //var client = new VidyoPortalAdminServicePortTypeClient();
            client.ClientCredentials.UserName.UserName = adminUser;
            client.ClientCredentials.UserName.Password = adminPassword;

            var request = new GetMembersRequest { Filter = new VidyoPortalAdminService.Filter { query = userName } };
            var result = await client.getMembersAsync(request);
            var members = result.GetMembersResponse.member;


        }
    }
}