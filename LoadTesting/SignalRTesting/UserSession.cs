﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.SignalR.Client;

namespace SignalRTesting
{
    public class UserSession
    {
        private const string CommandHubName = "CommandHub";
        private readonly HubConnection _connection;
        private Guid BrowserGuid { get; } = Guid.NewGuid();

        public UserSession(string url)
        {
            _connection = new HubConnection(url);



            IHubProxy proxy = _connection.CreateHubProxy(BrowserGuid.ToString());
        }

        public void Connect()
        {

        }

        private static Command CreateUserIsConnectedCommand(string userName, string password, string guid)
        {
            return new Command
            {
                Id = CommandId.II_CLIENT_IN_EVENT_USER_IS_CONNECTED,
                Data = new Dictionary<string, string>
                {
                    { "username", userName },
                    { "password", password },
                    { "browserGuid", guid },
                }
            };
        }

        private static Command CreateLoginCommand(string userName, string password, string guid)
        {
            return new Command
            {
                Id = CommandId.II_CLIENT_IN_EVENT_LOGIN,
                Data = new Dictionary<string, string>
                {
                    { "username", userName },
                    { "password", password },
                    { "browserGuid", guid },
                    { "clientType", "Web" },
                    { "version", "1.2.3" },
                },
            };
        }

    }
}